Simulation Development Tools
============================

Copyright (c) 2014-2015 Modelon GmbH

The Simulation Development Tools are a set of free libraries based around the [Scientific Data Format](ScientificDataFormat.md) that provides data exchange and interpolation functions for a range of simulation and programming environments.
