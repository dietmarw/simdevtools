/**
 * Copyright (C) 2014-2015 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the PYTHON_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// PYTHON_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef _WIN32
#define PYTHON_API __declspec(dllexport)
#else
#define PYTHON_API
#endif

#include "NDTable.h"

PYTHON_API double NDTable_add(double a, double b);

PYTHON_API ModelicaNDTable_h create_table(int ndims, const int *dims, const double **scales, const double *data);

PYTHON_API void close_table(ModelicaNDTable_h table);

PYTHON_API int evaluate(
	ModelicaNDTable_h table,
	int ndims,
	const double **params,
	ModelicaNDTable_InterpMethod_t interp_method,
	ModelicaNDTable_ExtrapMethod_t extrap_method,
	int nvalues,
	double *values);

PYTHON_API int evaluate_derivative(
	ModelicaNDTable_h table, 
	int nparams, 
	const double params[],
	const double delta_params[],
	ModelicaNDTable_InterpMethod_t interp_method,
	ModelicaNDTable_ExtrapMethod_t extrap_method, 
	double *value);