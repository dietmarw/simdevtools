/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#ifndef HDF5RECORDER_H_
#define HDF5RECORDER_H_

#include "ModelicaHDF5Recorder.h"

#ifdef __cplusplus
extern "C" {
#endif

EXTERN_API ModelicaHDF5Recorder_recorder_h HDF5Recorder_open(
	const char *filename,
	int num_signals,
	const char **signal_names, 
	const char **signal_quantities, 
	const char **signal_units,
	const char **signal_display_units,
	const char **signal_comments, 
	const char *sdf_filename);

EXTERN_API int HDF5Recorder_record(ModelicaHDF5Recorder_recorder_h recorder, double time, int num_signals, const double *signal_values);

EXTERN_API int HDF5Recorder_close(ModelicaHDF5Recorder_recorder_h recorder);

#ifdef __cplusplus
}
#endif

#endif /*HDF5RECORDER_H_*/