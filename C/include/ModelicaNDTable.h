/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#ifndef MODELICANDTABLE_H_
#define MODELICANDTABLE_H_

#if defined(__cplusplus)
	#define MODELICA_NDTABLE_API extern "C"
#else
	#define MODELICA_NDTABLE_API extern
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*! The maximum number of dimensions allowed by HDF5 */
#define MAX_NDIMS 32

/*! Interpolation methods */
typedef enum {
	NDTABLE_INTERP_NEAREST = 1,
	NDTABLE_INTERP_LINEAR,
	NDTABLE_INTERP_AKIMA
} ModelicaNDTable_InterpMethod_t;

/*! Extrapolation methods */
typedef enum {
    NDTABLE_EXTRAP_HOLD = 1,
	NDTABLE_EXTRAP_LINEAR,
	NDTABLE_EXTRAP_NONE
} ModelicaNDTable_ExtrapMethod_t;

/*! The structure that holds the data values and meta info of a table */
typedef struct {
	char *		filename;					//!< the name of the file from which the dataset has been loaded e.g. "C:\data.h5"
	char *		datasetname;				//!< the name of the dataset e.g. "/G1/DS1"
	int			ndims;						//!< the number of dimensions of the table
	int			dims[MAX_NDIMS];			//!< extents of the dimensions
	int			numel;						//!< the number of data values
	int 		offs[MAX_NDIMS];			//!< the index offsets for the dimensions
	double *	data;						//!< the data values
	double *	scales[MAX_NDIMS];			//!< array of pointers to the scale values
	char *		data_quantity;				//!< the quantity of the data
	char *		data_unit;					//!< the unit of the data
	char *		scale_quantities[MAX_NDIMS];//!< the quantities of the scales
	char *		scale_units[MAX_NDIMS];		//!< the units of the scales
} ModelicaNDTable_t;

typedef ModelicaNDTable_t * ModelicaNDTable_h;

MODELICA_NDTABLE_API ModelicaNDTable_h ModelicaNDTable_open(
	const char *filename, 
	const char *datasetname, 
	const int ndims, 
	const char *data_quantity,
	const char *data_unit,
	const char **scale_quantities,
	const char **scale_units,
	int share);

MODELICA_NDTABLE_API void ModelicaNDTable_evaluate(
	ModelicaNDTable_h table,
	int nparams, 
	const double params[], 
	ModelicaNDTable_InterpMethod_t interp_method,
	ModelicaNDTable_ExtrapMethod_t extrap_method, 
	double *value);

MODELICA_NDTABLE_API void ModelicaNDTable_evaluate_derivative(
	ModelicaNDTable_h table,
	int nparams, 
	const double params[], 
	const double delta_params[], 
	ModelicaNDTable_InterpMethod_t interp_method,
	ModelicaNDTable_ExtrapMethod_t extrap_method,
	double *value);

MODELICA_NDTABLE_API void ModelicaNDTable_close(ModelicaNDTable_h table);

#ifdef __cplusplus
}
#endif

#endif /*MODELICANDTABLE_H_*/
