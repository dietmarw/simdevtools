/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#include "gtest/gtest.h"
#include "NDTable.h"


TEST(CoreTest, findUserTab) {
	//char *scaleQuantities[] = {"SQ1", "SQ2"};
	//char *scaleUnits[]		= {"SU1", "SU2"};
	ModelicaNDTable_h ds = NDTable_find_usertab(/*userTabs, N_USERTABS,*/ "usertab.sdf", "/DS1");
	//EXPECT_EQ(&userTabs[0], ds);
	EXPECT_DOUBLE_EQ(0.0, ds->data[0]);
}
