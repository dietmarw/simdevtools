/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#include "gtest/gtest.h"
#include "HDF5Recorder.h"


TEST(RecordingTest, WriteFile) {
	char *signal_names[3]		= {"DS1", "DS2", "DS3"};
	char *signal_quantities[3]	= {"Q1", "Q2", "Q3"};
	char *signal_units[3]		= {"U1", "U2", "U3"};
	char *signal_comments[3]	= {"Signal 1", "Signal 2", "Signal 3"};
	double values[3] = {1,2,3};
	int i;

	ModelicaHDF5Recorder_recorder_h recorder = ModelicaHDF5Recorder_open("recorder.h5", 3, (const char **)signal_names, (const char **)signal_quantities, (const char **)signal_units, (const char **)signal_comments, "recorder.sdf");

	EXPECT_TRUE(recorder != NULL);

	for(i = 0; i < 10; i++) {
		values[0] = i + 0.1;
		values[1] = i + 0.2;
		values[2] = i + 0.3;
		ModelicaHDF5Recorder_record(recorder, i * 0.01, 3, values);
	}

	ModelicaHDF5Recorder_close(recorder);
}