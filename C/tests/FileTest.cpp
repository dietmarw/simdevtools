/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#include "gtest/gtest.h"

#include "NDTable.h"

#define GROUPS_FILE "groups.sdf"
#define DATA_FILE "datasets.sdf"
#define DS1 "/ds1"
#define DS2 "/ds2"
#define DS3 "/ds3"

static char error_message[MAX_MESSAGE_LENGTH];


TEST(FileTest, ReadMissingDataset) {
	EXPECT_TRUE(ModelicaNDTable_open("missing_quantity.sdf", "/DS2", 1, NULL, NULL, NULL, NULL, 0) == NULL);
	//NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	//EXPECT_STREQ("Dataset '/DS2' does not exist in 'missing_quantity.sdf'", error_message);
}

TEST(FileTest, ScaleQuantities) {
	char *scaleQuantities[1] = { "XXX" };
	EXPECT_TRUE(ModelicaNDTable_open("scales.sdf", "/DS2", 1, NULL, NULL, (const char **)scaleQuantities, NULL, 0) == NULL);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("The scale for dimension 0 of dataset '/DS2' in 'scales.sdf' has the wrong quantity. Expected 'XXX' but was 'SQ2'.", error_message);

	scaleQuantities[0] = "SQ2";
	EXPECT_TRUE(ModelicaNDTable_open("scales.sdf", "/DS2", 1, NULL, NULL, (const char **)scaleQuantities, NULL, 0) != NULL);
}

TEST(FileTest, ScaleUnits) {
	char *scaleUnits[1] = { "XXX" };

	EXPECT_TRUE(ModelicaNDTable_open("scales.sdf", "/DS2", 1, NULL, NULL, NULL, (const char **)scaleUnits, 0) == NULL);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("The scale for dimension 0 of dataset '/DS2' in 'scales.sdf' has the wrong unit. Expected 'XXX' but was 'SU2'.", error_message);

	scaleUnits[0] = "SU2";
	EXPECT_TRUE(ModelicaNDTable_open("scales.sdf", "/DS2", 1, NULL, NULL, NULL, (const char **)scaleUnits, 0) != NULL);
}
