/**
 * Copyright (C) 2014-2015 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#include <stdlib.h>
#include "Python.h"


PYTHON_API double NDTable_add(double a, double b) {
	return a + b;
}

PYTHON_API ModelicaNDTable_h create_table(int ndims, const int *dims, const double *data, const double **scales) {
	int i, j;
	ModelicaNDTable_h table = NDTable_alloc_table();

	table->ndims = ndims;

	for(i = 0; i < ndims; i++) {
		table->dims[i] = dims[i];
		table->scales[i] = (double *)malloc(sizeof(double) * dims[i]);
		for(j = 0; j < dims[i]; j++) {
			table->scales[i][j] = scales[i][j];
		}
	}

	table->numel = NDTable_calculate_numel(table->ndims, table->dims);
	
	table->data = (double *)malloc(sizeof(double) * table->numel);
	for(i = 0; i < table->numel; i++) {
		table->data[i] = data[i];
	}

	return table;
}

PYTHON_API void close_table(ModelicaNDTable_h table) {
	int i;

	free(table->data);

	for(i = 0; i < MAX_NDIMS; i++) {
		free(table->scales[i]);
	}

	free(table);
}

PYTHON_API int evaluate(
	ModelicaNDTable_h table,
	int ndims,
	const double **params,
	ModelicaNDTable_InterpMethod_t interp_method,
	ModelicaNDTable_ExtrapMethod_t extrap_method,
	int nvalues,
	double *values) {

	int i, j, status;
	double params_[32];

	for(i = 0; i < nvalues; i++) {
		
		for(j = 0; j < ndims; j++) {
			params_[j] = params[j][i];
		}

		if(NDTable_evaluate(table, ndims, params_, interp_method, extrap_method, &values[i]) != NDTABLE_INTERPSTATUS_OK) {
			return -1;
		}
	}

	return 0;
}

PYTHON_API int evaluate_derivative(
	ModelicaNDTable_h table, 
	int nparams, 
	const double params[],
	const double delta_params[],
	ModelicaNDTable_InterpMethod_t interp_method,
	ModelicaNDTable_ExtrapMethod_t extrap_method, 
	double *value) {

		return NDTable_evaluate_derivative(table, nparams, params, delta_params, interp_method, extrap_method, value);
}
