/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#define S_FUNCTION_NAME sfun_hdf5recorder
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include "HDF5Recorder.h"

#define MAX_MESSAGE_LENGTH 256

static char	message[MAX_MESSAGE_LENGTH];

static char * getStringParam(SimStruct *S, int index) {
	const mxArray	*pa_param	= NULL;
	char			*param		= NULL;
	size_t			 nchars;

	pa_param = ssGetSFcnParam(S, index);
	if(mxIsChar(pa_param)) {
		nchars = mxGetNumberOfElements(pa_param) + 1;
		param = (char *) mxCalloc(nchars, sizeof(char));
		mxGetString(pa_param, param, nchars);
		return param;
	}

	return NULL;
}

static char ** getStringArrayParam(SimStruct *S, int index) {
	const mxArray	*pa_param	= NULL;
	const mxArray	*pa_cell	= NULL;
	char		   **param		= NULL;
	size_t			 nchars;
	size_t			 ncells;
	int i;

	pa_param = ssGetSFcnParam(S, index);
	if(mxIsCell(pa_param)) {
		ncells = mxGetNumberOfElements(pa_param);
		param = (char **) mxMalloc(ncells * sizeof(char *));
		
		for(i = 0; i < (int)ncells; i++) {
			pa_cell = mxGetCell(pa_param, i);
			if(mxIsChar(pa_cell)) {
				nchars = mxGetNumberOfElements(pa_cell) + 1;
				param[i] = (char *) mxCalloc(nchars, sizeof(char));
				mxGetString(pa_cell, param[i], nchars);
			} else {
				param[i] = NULL;
			}
		}

		return param;
	}

	return NULL;
}

static double getScalarParam(SimStruct *S, int index) {
	const mxArray *pa_param = ssGetSFcnParam(S, index);

	if(mxIsNumeric(pa_param)) {
		return mxGetScalar(pa_param);
	}

	return 0;
}


static void mdlInitializeSizes(SimStruct *S) {
	const mxArray * pa_nsig	= NULL;
	mxChar *		str			= NULL;
	size_t			i			= 0;
	//size_t			nchars		= 0;
	int				nsig		= -1;
	
	ssSetNumSFcnParams(S, 8);
	if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
		return; // Parameter mismatch reported by the Simulink engine
	}

	ssSetNumIWork(S, 1);
	ssSetNumPWork(S, 1);

	// get the number of dimensions
	pa_nsig = ssGetSFcnParam(S, 0);
	if(mxIsNumeric(pa_nsig)) {
		nsig = (int) mxGetScalar(pa_nsig); 
	} else {
		sprintf(message, "Parameter 1 must be an int specifying the number of signals.");
		ssSetErrorStatus(S, message);
	}

	if(nsig < 1) {
		sprintf(message, "The number of signals must be greater than 1.");
		ssSetErrorStatus(S, message);
	}

	if (!ssSetNumInputPorts(S, 1)) return;

	ssSetInputPortWidth(S, 0, nsig);
	ssSetInputPortDirectFeedThrough(S, 0, 1);
	ssSetInputPortRequiredContiguous(S, 0, 1);
	
	if (!ssSetNumOutputPorts(S,0)) return;
	
	ssSetNumSampleTimes(S, 1);

	/* Take care when specifying exception free code - see sfuntmpl.doc */
	ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}

static void mdlInitializeSampleTimes(SimStruct *S) {
	ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
	ssSetOffsetTime(S, 0, 0.0);
}

#define MDL_START
#if defined(MDL_START) 
static void mdlStart(SimStruct *S) {
	const mxArray * pa_nsig	= NULL;
	const mxArray * pa_result_file_name	 = NULL;
	const mxArray * pa_convert_to_sdf	 = NULL;
	const mxArray * pa_sdf_file_name	 = NULL;
	const mxArray * pa_signal_quantities = NULL;
	const mxArray * pa_cell				= NULL;
	size_t			nchars				= 0;
	size_t			ncells				= 0;

	char *			result_file_name	= NULL;
	char *			sdf_file_name		= NULL;
	int				convert_to_sdf		= 0;

	char **			signal_names		= NULL;
	char **			signal_quantities	= NULL;
	char **			signal_units		= NULL;
	char **			signal_comments		= NULL;
	int status;

	int_T *			nsig		= ssGetIWork(S);
	ModelicaHDF5Recorder_recorder_h *	recorder	= (ModelicaHDF5Recorder_recorder_h *)ssGetPWork(S);

	// get the number of dimensions
	pa_nsig = ssGetSFcnParam(S, 0);
	if(mxIsNumeric(pa_nsig)) {
		nsig[0] = (int) mxGetScalar(pa_nsig);
	} else {
		sprintf(message, "Parameter 1 must be an int specifying the number of dimensions.");
		ssSetErrorStatus(S, message);
		goto out;
	}

	// get the result file name
	pa_result_file_name = ssGetSFcnParam(S, 1);
	if(mxIsChar(pa_result_file_name)) {
		nchars = mxGetNumberOfElements(pa_result_file_name) + 1;
		result_file_name = (char *) mxCalloc(nchars, sizeof(char));
		mxGetString(pa_result_file_name, result_file_name, nchars);
	} else {
		ssSetErrorStatus(S, "Parameter 2 must be a string");
		goto out;
	}

	// get the convert to SDF flag
	pa_convert_to_sdf = ssGetSFcnParam(S, 2);
	if(mxIsNumeric(pa_convert_to_sdf)) {
		convert_to_sdf = (int) mxGetScalar(pa_convert_to_sdf);
	} else {
		sprintf(message, "Parameter 3 must be a logical scalar.");
		ssSetErrorStatus(S, message);
		goto out;
	}

	// get the sdf file name
	pa_sdf_file_name = ssGetSFcnParam(S, 3);
	if(mxIsChar(pa_sdf_file_name)) {
		nchars = mxGetNumberOfElements(pa_sdf_file_name) + 1;
		sdf_file_name = (char *) mxCalloc(nchars, sizeof(char));
		mxGetString(pa_sdf_file_name, sdf_file_name, nchars);
	} else {
		ssSetErrorStatus(S, "Parameter 4 must be a string");
		goto out;
	}

	signal_names		= getStringArrayParam(S, 4);
	signal_quantities	= getStringArrayParam(S, 5);
	signal_units		= getStringArrayParam(S, 6);
	signal_comments		= getStringArrayParam(S, 7);

	*recorder = HDF5Recorder_open(result_file_name, nsig[0], signal_names, signal_quantities, signal_units, signal_comments, convert_to_sdf ? sdf_file_name : NULL);
	
	if(*recorder == NULL) {
		ssSetErrorStatus(S, "Failed to open recorder");
	}

	/*
	// get the scale quantities
	pa_signal_quantities = ssGetSFcnParam(S, 5);
	if(mxIsCell(pa_signal_quantities)) {
		ncells = mxGetNumberOfElements(pa_signal_quantities);
		if(ncells != nsig[0]) {
			ssSetErrorStatus(S, "Parameter 5 must contain one string for every dimension");
			//goto out;
		}
		for(i = 0; i < (int)ncells; i++) {
			pa_cell = mxGetCell(pa_signal_quantities, i);
			if(mxIsChar(pa_cell)) {
				nchars = mxGetNumberOfElements(pa_cell) + 1;
				pa_signal_quantities[i] = (char *) mxCalloc(nchars, sizeof(char));
				mxGetString(pa_cell, pa_signal_quantities[i], nchars);
			} else {
				ssSetErrorStatus(S, "Parameter 6 contains non-string values");
				goto out;
			}
		}
	} else {
		ssSetErrorStatus(S, "Parameter 6 must be a cell array of strings");
		goto out;
	}
	*/

out:
	mxFree(result_file_name);
	mxFree(sdf_file_name);
}
#endif /*  MDL_START */


static void mdlOutputs(SimStruct *S, int_T tid) {
	time_T  t = ssGetT(S);
	const real_T *u = ssGetInputPortRealSignal(S, 0);
	ModelicaHDF5Recorder_recorder_h recorder = (ModelicaHDF5Recorder_recorder_h)ssGetPWork(S)[0];
	
	int_T nsig = ssGetInputPortDimensionSize(S, 0, 0);

	if (HDF5Recorder_record(recorder, t, nsig, u) != 0) {
		ssSetErrorStatus(S, "Failed to record signals");
	}
}


static void mdlTerminate(SimStruct *S) {
	char *	result_file_name	= NULL;
	char *	sdf_file_name		= NULL;
	int		convert_to_sdf		= (int) getScalarParam(S, 2);
	ModelicaHDF5Recorder_recorder_h recorder = (ModelicaHDF5Recorder_recorder_h)ssGetPWork(S)[0];

	if(HDF5Recorder_close(recorder) != 0) {
		ssSetErrorStatus(S, "Failed to close recorder");
	}

	mxFree(result_file_name);
	mxFree(sdf_file_name);
}

#ifdef MATLAB_MEX_FILE /* Is this file being compiled as a MEX-file? */
#include "simulink.c" /* MEX-file interface mechanism */
#else
#include "cg_sfun.h" /* Code generation registration function */
#endif
