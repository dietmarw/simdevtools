#include "ModelicaUtilities.h"
#include "ModelicaNDTable.h"
#include "NDTable.h"

static char error_message[MAX_MESSAGE_LENGTH] = "No error";

ModelicaNDTable_h ModelicaNDTable_open(
	const char *filename, 
	const char *datasetname, 
	const int ndims, 
	const char *data_quantity,
	const char *data_unit, 
	const char **scale_quantities, 
	const char **scale_units, 
	int share) {

	ModelicaNDTable_h table;

#ifdef _DEBUG
	ModelicaFormatMessage("ModelicaNDTable_open('%s', '%s', %d, ..., %d)\n", filename, datasetname, ndims, share);
#endif

	table = NDTable_read(filename, datasetname, ndims, data_quantity, data_unit, scale_quantities, scale_units, share);

	if(table == NULL)
		ModelicaError(NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message));

	return table;
}

void ModelicaNDTable_evaluate(
	ModelicaNDTable_h table, 
	int nparams, 
	const double params[], 
	ModelicaNDTable_InterpMethod_t interp_method,
	ModelicaNDTable_ExtrapMethod_t extrap_method,
	double *value) {

#ifdef _DEBUG
	ModelicaFormatMessage("ModelicaNDTable_evaluate(0x%p, %d, ..., %d, %d, ...)\n", table, nparams, interp_method, extrap_method);
#endif

	if(NDTable_evaluate(table, nparams, params, interp_method, extrap_method, value) < 0)
		ModelicaMessage(NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message));
}

void ModelicaNDTable_evaluate_derivative(
	ModelicaNDTable_h table,
	int nparams, 
	const double params[], 
	const double delta_params[], 
	ModelicaNDTable_InterpMethod_t interp_method,
	ModelicaNDTable_ExtrapMethod_t extrap_method, 
	double *value) {

#ifdef _DEBUG
	ModelicaFormatMessage("ModelicaNDTable_evaluate_derivative(0x%p, %d, ..., %d, %d, ...)\n", table, nparams, interp_method, extrap_method);
#endif

	if(NDTable_evaluate_derivative(table, nparams, params, delta_params, interp_method, extrap_method, value) < 0)
		ModelicaMessage(NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message));
}

void ModelicaNDTable_close(ModelicaNDTable_h table) {

#ifdef _DEBUG
	ModelicaFormatMessage("ModelicaNDTable_close(0x%p)\n", table);
#endif

	// TODO: Check if the table is shared and free if possible
}