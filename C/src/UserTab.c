/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#include <string.h>

#include "UserTab.h"


MODELICA_NDTABLE_API ModelicaNDTable_h NDTable_find_usertab(const char *filename, const char *datasetname) {
	int i;
	ModelicaNDTable_h ds;

	if(filename == NULL || strlen(filename) < 1) {
		return NULL;
	}

	if(datasetname == NULL || strlen(datasetname) < 1) {
		return NULL;
	}

	for(i = 0; i < N_USERTABS; i++) {
		ds = &userTabs[i];
		if(ds->filename && strcmp(filename, ds->filename) == 0 && ds->datasetname && strcmp(datasetname, ds->datasetname) == 0) {
			return ds;
		}
	}

	return NULL;
}
