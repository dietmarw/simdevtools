/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "hdf5.h"
#include "hdf5_hl.h"

#include "ModelicaUtilities.h"

#include "ModelicaHDF5Functions.h"

// TODO: move this to shared header file
#define COMMENT_ATTR_NAME "COMMENT"
#define UNIT_ATTR_NAME "UNIT"
#define QUANTITY_ATTR_NAME "QUANTITY"
#define MAX_MESSAGE_LENGTH 4096


static char * format_message(const char *msg, ...) {
	va_list vargs;
	char *error_message = ModelicaAllocateStringWithErrorReturn(MAX_MESSAGE_LENGTH);
	
	va_start(vargs, msg);
	
	if(error_message == NULL)
		return NULL;
	
	vsprintf(error_message, msg, vargs);
	va_end(vargs);
	
	return error_message;
}

static char * assert_string_attribute(hid_t loc_id, const char *obj_name, const char *attr_name, const char *attr_value) {
	H5T_class_t type_class = H5T_NO_CLASS;
	size_t type_size = 0;
	int rank = -1;
	char *buffer = NULL;
	size_t len1, len2;
	char * qualified_name = (char *)obj_name;
	char name_buffer[100];
	char *error_msg = NULL;
	
	// get a proper name for the error message
	if (strcmp(obj_name, ".") == 0) {
		H5Iget_name(loc_id, name_buffer, 100);
		qualified_name = name_buffer;
	}

	if (H5LTget_attribute_info(loc_id, obj_name, attr_name, NULL, &type_class, &type_size) != 0) {
		error_msg = format_message("Missing required attribute '%s' in '%s' (expected '%s')", attr_name, qualified_name, attr_value);
		goto out;
	}

	if (H5LTget_attribute_ndims(loc_id, obj_name, attr_name, &rank) != 0) {
		error_msg = format_message("Failed to retrieve dimensions for attribute '%s' in '%s' (expected '%s')", attr_name, qualified_name, attr_value);
		goto out;
	}

	// check the rank
	if (rank > 1) {
		error_msg = format_message("Expected rank less or equal 1 for attribute '%s' in '%s' but was %d", attr_name, qualified_name, rank);
		goto out;
	}

	// check the type
	if (type_class != H5T_STRING) {
		error_msg = format_message("Attribute '%s' in '%s' has the wrong type. Expected H5T_STRING (=5) but was %d.", attr_name, qualified_name, type_class);
		goto out;
	}

	buffer = (char *)calloc((type_size), sizeof(char));

	if (H5LTget_attribute_string(loc_id, obj_name, attr_name, buffer) != 0) {
		error_msg = format_message("Failed to read attribute '%s' in '%s'", attr_name, qualified_name);
		goto out;
	}

	// compare the lengths first (HDF5 strings sometimes come in Fortran format)
	len1 = strlen(attr_value);
	len2 = strlen(buffer);
    if(len2 > type_size)
        len2 = type_size;
	if (len1 != len2 || strncmp(attr_value, buffer, type_size) != 0) {
		error_msg = format_message("Attribute '%s' in '%s' has the wrong value. Expected '%s' but was '%.*s'.", attr_name, qualified_name, attr_value, type_size, buffer);
	}

out:
	free(buffer);

	return error_msg;
}

void ModelicaHDF5Functions_create_group(const char *filename, const char *group_name, const char *comment) {
	hid_t file_id = H5I_INVALID_HID;
	hid_t group_id = H5I_INVALID_HID;
	char *error_msg = NULL;

	// open the file
	if ((file_id = H5Fopen(filename, H5F_ACC_RDWR, H5P_DEFAULT)) < 0) {
		// create a new one if it does not exist
		if ((file_id = H5Fcreate(filename, H5F_ACC_EXCL, H5P_DEFAULT, H5P_DEFAULT)) < 0) { 
			error_msg = format_message("Failed to create file '%s'", filename); 
			goto out;
		}
	}

	// create the group
	if((group_id = H5Gopen2(file_id, group_name, H5P_DEFAULT)) < 0) {
		// create the group if it does not exist
		if((group_id = H5Gcreate2(file_id, group_name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)) < 0) { 
			error_msg = format_message("Failed to create group '%s' in '%s'", group_name, filename);
			goto out; 
		}
	}

	if(comment && strlen(comment) > 0) {
		if(H5LTset_attribute_string(group_id, ".", COMMENT_ATTR_NAME, comment) != 0) { 
			error_msg = format_message("Failed to set comment for group '%s' in '%s'", group_name, filename);
		}
	}

out:
	if (group_id >= 0) { H5Gclose(group_id); }
	if (file_id >= 0) {	H5Fclose(file_id); }

	if(error_msg) ModelicaError(error_msg);
}

void ModelicaHDF5Functions_get_dataset_dims(const char *filename, const char *dataset_name, int dims[]) {
	hid_t file_id = H5I_INVALID_HID;
	H5T_class_t type_class;
	size_t size;
	hsize_t dimsbuf[32];
	int i;
	int ndims;
	char *error_msg = NULL;

	if ((file_id = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT)) < 0) {
		error_msg = format_message("Failed to create file '%s'", filename);
		goto out;
	}

	if (H5LTget_dataset_ndims(file_id, dataset_name, &ndims) != 0) {
		error_msg = format_message("Failed to open dataset '%s' in '%s'", dataset_name, filename);
		goto out;
	}

	if (H5LTget_dataset_info(file_id, dataset_name, dimsbuf, &type_class, &size) != 0) {
		error_msg = format_message("Failed to open dataset '%s' in '%s'", dataset_name, filename);
		goto out;
	}

	for (i = 0; i < ndims; i++) {
		dims[i] = (int)dimsbuf[i];
	}

out:
	if (file_id >= 0) H5Fclose(file_id);

	if(error_msg) ModelicaError(error_msg);
}

void ModelicaHDF5Functions_read_dataset_double(const char *filename, const char *dataset_name, const char *quantity, const char *unit, double *buffer) {
	hid_t file_id = H5I_INVALID_HID;
	char *error_msg = NULL;

	if ((file_id = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT)) < 0) {
		error_msg = format_message("Failed to open '%s'", filename);
		goto out;
	}

	if (H5LTread_dataset_double(file_id, dataset_name, buffer) != 0) {
		error_msg = format_message("Failed to read double dataset '%s' from '%s'", dataset_name, filename);
		goto out;
	}

	// check the unit
	if (unit != NULL && strlen(unit) > 0 && (error_msg = assert_string_attribute(file_id, dataset_name, UNIT_ATTR_NAME, unit)) != NULL) {
		goto out;
	}

	// check the quantity
	if (quantity != NULL && strlen(quantity) > 0 && (error_msg = assert_string_attribute(file_id, dataset_name, QUANTITY_ATTR_NAME, quantity)) != NULL) {
		goto out;
	}

out:
	if (file_id >= 0) H5Fclose(file_id);

	if(error_msg) ModelicaError(error_msg);
}

void ModelicaHDF5Functions_read_dataset_int(const char *filename, const char *dataset_name, const char *quantity, const char *unit, int *buffer) {
	hid_t file_id = H5I_INVALID_HID;
	char *error_msg = NULL;

	if ((file_id = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT)) < 0) {
		error_msg = format_message("Failed to open '%s'", filename);
		goto out;
	}

	if (H5LTread_dataset_int(file_id, dataset_name, buffer) != 0) {
		error_msg = format_message("Failed to read integer dataset '%s' from '%s'", dataset_name, filename);
		goto out;
	}

	// check the unit
	if (unit != NULL && strlen(unit) > 0 && (error_msg = assert_string_attribute(file_id, dataset_name, UNIT_ATTR_NAME, unit)) != NULL) {
		goto out;
	}

	// check the quantity
	if (quantity != NULL && strlen(quantity) > 0 && (error_msg = assert_string_attribute(file_id, dataset_name, QUANTITY_ATTR_NAME, quantity)) != NULL) {
		goto out;
	}

out:
	if (file_id >= 0) H5Fclose(file_id);

	if(error_msg) ModelicaError(error_msg);
}

void ModelicaHDF5Functions_make_dataset_double(const char *filename, const char *dataset_name, int ndims, const int dims[], const double *buffer, const char *quantity, const char *unit, const char *comment) {
	hid_t file_id = H5I_INVALID_HID;
	int i;
	hsize_t dimsbuf[32];
	int rank = -1;

	for (i = 0; i < ndims; i++) {
		dimsbuf[i] = (hsize_t)dims[i];
	}

	// open the file
	if ((file_id = H5Fopen(filename, H5F_ACC_RDWR, H5P_DEFAULT)) < 0) {
		// create a new one if it does not exist
		if ((file_id = H5Fcreate(filename, H5F_ACC_EXCL, H5P_DEFAULT, H5P_DEFAULT)) < 0) {
			goto out;
		}
	}

	// delete the dataset if it already exists
	if (H5LTget_dataset_ndims(file_id, dataset_name, &rank) == 0) {
		if (H5Ldelete(file_id, dataset_name, H5P_DEFAULT) != 0) {
			// TODO: set error message
			goto out;
		}
	}

	if (H5LTmake_dataset_double(file_id, dataset_name, ndims, dimsbuf, buffer) != 0) {
		goto out;
	}

	// set the unit
	if (unit != NULL && strlen(unit) > 0) {
		if (H5LTset_attribute_string(file_id, dataset_name, UNIT_ATTR_NAME, unit) != 0) {
			goto out;
		}
	}

	// set the quantity
	if (quantity != NULL && strlen(quantity) > 0) {
		if (H5LTset_attribute_string(file_id, dataset_name, QUANTITY_ATTR_NAME, quantity) != 0) {
			goto out;
		}
	}

	// set the comment
	if (comment != NULL && strlen(comment) > 0) {
		if (H5LTset_attribute_string(file_id, dataset_name, COMMENT_ATTR_NAME, comment) != 0) {
			goto out;
		}
	}

out:
	if (file_id >= 0) H5Fclose(file_id);
}

void ModelicaHDF5Functions_make_dataset_int(const char *filename, const char *dataset_name, int ndims, const int dims[], const int *buffer, const char *quantity, const char *unit, const char *comment) {
	hid_t file_id = H5I_INVALID_HID;
	int i;
	hsize_t dimsbuf[32];
	int rank = -1;

	for (i = 0; i < ndims; i++) {
		dimsbuf[i] = (hsize_t)dims[i];
	}

	// open the file
	if ((file_id = H5Fopen(filename, H5F_ACC_RDWR, H5P_DEFAULT)) < 0) {
		// create a new one if it does not exist
		if ((file_id = H5Fcreate(filename, H5F_ACC_EXCL, H5P_DEFAULT, H5P_DEFAULT)) < 0) {
			goto out;
		}
	}

	// delete the dataset if it already exists
	if (H5LTget_dataset_ndims(file_id, dataset_name, &rank) == 0) {
		if (H5Ldelete(file_id, dataset_name, H5P_DEFAULT) != 0) {
			goto out;
		}
	}

	if (H5LTmake_dataset_int(file_id, dataset_name, ndims, dimsbuf, buffer) != 0) {
		goto out;
	}

	// set the unit
	if (unit != NULL && strlen(unit) > 0) {
		if (H5LTset_attribute_string(file_id, dataset_name, UNIT_ATTR_NAME, unit) != 0) {
			goto out;
		}
	}

	// set the quantity
	if (quantity != NULL && strlen(quantity) > 0) {
		if (H5LTset_attribute_string(file_id, dataset_name, QUANTITY_ATTR_NAME, quantity) != 0) {
			goto out;
		}
	}

	// set the comment
	if (comment != NULL && strlen(comment) > 0) {
		if (H5LTset_attribute_string(file_id, dataset_name, COMMENT_ATTR_NAME, comment) != 0) {
			goto out;
		}
	}

out:
	if (file_id >= 0) {
		H5Fclose(file_id);
	}
}

void ModelicaHDF5Functions_attach_scale(const char *filename, const char *dataset_name, const char *scale_name, const char *dim_name, int dim) {
	hid_t file_id	 = H5I_INVALID_HID;
	hid_t dataset_id = H5I_INVALID_HID;
	hid_t scale_id	 = H5I_INVALID_HID;
	char *error_msg = NULL;

	if ((file_id = H5Fopen(filename, H5F_ACC_RDWR, H5P_DEFAULT)) < 0) {
		error_msg = format_message("Failed to open '%s'", dataset_name, filename);
		goto out;
	}

	if ((dataset_id = H5Dopen2(file_id, dataset_name, H5P_DEFAULT)) < 0) {
		error_msg = format_message("Failed to open dataset '%s'", dataset_name);
		goto out;
	}

	if ((scale_id = H5Dopen2(file_id, scale_name, H5P_DEFAULT)) < 0) {
		error_msg = format_message("Failed to open dataset '%s'", scale_name);
		goto out;
	}

	if (H5DSset_scale(scale_id, dim_name) < 0) {
		error_msg = format_message("Failed to set scale on '%s'", scale_name);
		goto out;
	}

	if (H5DSattach_scale(dataset_id, scale_id, dim) < 0) {
		error_msg = format_message("Failed to attach scale");
		goto out;
	}

out:
	if (scale_id > 0) H5Dclose(scale_id);
	if (dataset_id > 0) H5Dclose(dataset_id);
	if (file_id > 0) H5Fclose(file_id);

	if(error_msg) ModelicaError(error_msg);
}
