#include <stdlib.h>

extern "C" {
#include "ModelicaUtilities.h"
}

/* This is a mockup for the Modelica Utilities */ 

void ModelicaMessage(const char *) {
	// do nothing
}

void ModelicaFormatMessage(const char *,...) {
	// do nothing
}

void ModelicaVFormatMessage(const char *, va_list) {
	// do nothing
}

void ModelicaError(const char *) {
	// do nothing
}

void ModelicaFormatError(const char *,...) {
	// do nothing
}

void ModelicaVFormatError(const char *, va_list) {
	// do nothing
}

char* ModelicaAllocateString(size_t len) {
	return (char *)malloc(len);
}


char* ModelicaAllocateStringWithErrorReturn(size_t len) {
	return (char *)malloc(len);
}
