# Copyright (C) 2014 Modelon GmbH. All rights reserved.
#
# This file is part of the Simulation Development Tools.
#
# This program and the accompanying materials are made
# available under the terms of the BSD 3-Clause License
# which accompanies this distribution, and is available at
# http://simdevtools.org/LICENSE.txt
#
# Contributors:
#   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation

'''
Import data from an Excel sheet to SDF

@author: Torsten Sommer <torsten.sommer@modelon.com>
'''

import os.path
import xlrd
import numpy as np
import sdf


# name of the Excel file to import
filename = 'time_series.xlsx'

# open the workbook
book = xlrd.open_workbook(filename)
    
# get the first sheet
sh = book.sheet_by_index(0)

# get the names, quantities and units
n_t = sh.cell_value(0, 1)
q_t = sh.cell_value(1, 1)
u_t = sh.cell_value(2, 1)

n_u = sh.cell_value(0, 2)
q_u = sh.cell_value(1, 2)
u_u = sh.cell_value(2, 2)

# get the data
col_t = sh.col_values(1, 3, sh.nrows)
col_u = sh.col_values(2, 3, sh.nrows)

# create the data arrays
t = np.array(col_t)
u = np.array(col_u)

# create the datasets
ds_t = sdf.Dataset(n_t, data=t, quantity=q_t, unit=u_t, is_scale=True, scale_name='Time')
ds_u = sdf.Dataset(n_u, data=u, quantity=q_u, unit=u_u, scales=[ds_t])

# create the root group
g = sdf.Group('/', comment='Imported from ' + filename, datasets=[ds_t, ds_u])

# change the file extension
outfile = os.path.splitext(filename)[0] + '.sdf'

# write the SDF file
sdf.save(outfile, g)
