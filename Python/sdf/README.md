To build the package using distutil:

Open a command line and change to the 'Python' folder. Enter:

> python setup.py sdist

This will build the package sdt-0.0.1.zip in the dist folder.

To install the package unpack the zip file. Change into the unzipped package folder and enter:

> python setup.py install

This will add the SDT library into your local Python installation.

