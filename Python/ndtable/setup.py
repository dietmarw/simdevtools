from distutils.core import setup

setup(name = 'NDTable',
      version = '0.0.1',
      description = 'Interpolate multi-dimensional data',
      author = 'Torsten Sommer',
      author_email = 'torsten.sommer(at)modelon.com',
      url = 'http://www.simdevtools.org/',
      license = 'Standard 3-clause BSD',
      packages = ['ndtable', 'ndtable.examples'],
      package_data = {'ndtable': ['*.dll']},
      long_description = \
"""
Multi-dimensional Interpolation for Python
==========================================

The NDTable package provides a lookup table to consistently inter- and
extrapolate arrays of arbitrary dimensions using different algorithms:

* Interpolation: `nearest`, `linear`, `akima`
* Extrapolation: `hold`, `linear`

It can handle non-finite numbers (NaN, +Inf, -Inf).
""",
      classifiers = ['Development Status :: 3 - Alpha',
                     'Intended Audience :: Developers',
                     'Intended Audience :: Information Technology',
                     'Intended Audience :: Science/Research',
                     'License :: OSI Approved :: BSD License',
                     'Operating System :: Microsoft :: Windows',
                     'Programming Language :: Python',
                     'Topic :: Software Development :: Libraries :: Python Modules',
                     'Topic :: Scientific/Engineering'],
      requires = ['numpy (>=1.6.1)'],
)
