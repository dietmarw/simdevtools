within SimDevTools.Internal.Tests;
model DummyDimensions
  extends Modelica.Icons.Example;

  NDTable table(
    interpMethod=SimDevTools.Types.InterpolationMethod.Linear,
    nin=2,
    dataset="/z",
    extrapMethod=SimDevTools.Types.ExtrapolationMethod.Nearest,
    filename=classDirectory() + "dummy_dim.sdf")
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Sources.Ramp ramp(
    duration=1,
    height=3,
    offset=-1.5)
    annotation (Placement(transformation(extent={{-60,10},{-40,30}})));
  Modelica.Blocks.Sources.Ramp ramp1(
    duration=2,
    height=3,
    offset=-1.5)
    annotation (Placement(transformation(extent={{-60,-30},{-40,-10}})));
equation
  connect(ramp.y, table.u[2]) annotation (Line(
      points={{-39,20},{-26,20},{-26,1},{-12,1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(ramp1.y, table.u[1]) annotation (Line(
      points={{-39,-20},{-26,-20},{-26,-1},{-12,-1}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end DummyDimensions;
