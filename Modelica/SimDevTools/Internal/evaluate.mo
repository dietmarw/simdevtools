within SimDevTools.Internal;
function evaluate
  input SimDevTools.Types.ExternalNDTable table;
  input Real[:] params;
  input SimDevTools.Types.InterpolationMethod interpMethod;
  input SimDevTools.Types.ExtrapolationMethod extrapMethod;
  output Real value;
  external "C" ModelicaNDTable_evaluate(table, size(params, 1), params, interpMethod, extrapMethod, value) annotation (
   Include="#include \"ModelicaNDTable.h\"",
   Library={"ModelicaNDTable", "libhdf5", "libhdf5_hl"},
   IncludeDirectory="modelica://SimDevTools/Resources/Include",
   LibraryDirectory="modelica://SimDevTools/Resources/Library");
end evaluate;
