within SimDevTools.Internal;
function recordSignals
  input SimDevTools.Types.ExternalHDF5Recorder recorder;
  input Real t;
  input Integer numSignals;
  input Real signalValues[numSignals];
  external "C" ModelicaHDF5Recorder_record(recorder, t, numSignals, signalValues) annotation (
   Include="#include \"ModelicaHDF5Recorder.h\"",
   Library={"ModelicaHDF5Recorder", "libhdf5", "libhdf5_hl"},
   IncludeDirectory="modelica://SimDevTools/Resources/Include",
   LibraryDirectory="modelica://SimDevTools/Resources/Library");
end recordSignals;
