within ;
package SimDevTools "Simulation Development Tools"
  extends Modelica.Icons.Package;








protected 
    function usertab
        external "C" HDF5Table_userTab();
        annotation(IncludeDirectory="modelica://SimDevTools/Resources/Include",
               Include = "#include \"usertab.c\"
                 void HDF5Table_userTab() {}
               ");
    end usertab;


  function getDatasetDims
    input String fileName;
    input String datasetName;
    output Integer dims[32];
  external "C" ModelicaHDF5Functions_get_dataset_dims(fileName, datasetName, dims) annotation (
    Include="#include \"ModelicaHDF5Functions.h\"",
    Library={"ModelicaHDF5Functions", "libhdf5", "libhdf5_hl"},
    IncludeDirectory="modelica://SimDevTools/Resources/Include",
    LibraryDirectory="modelica://SimDevTools/Resources/Library");
  end getDatasetDims;


  annotation (uses(Modelica(
          version="3.2.1")), Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {100,100}}), graphics={
        Rectangle(
          extent={{-76,-26},{80,-76}},
          lineColor={95,95,95},
          fillColor={235,235,235},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-76,24},{80,-26}},
          lineColor={95,95,95},
          fillColor={235,235,235},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-76,74},{80,24}},
          lineColor={95,95,95},
          fillColor={235,235,235},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-28,74},{-28,-76}},
          color={95,95,95}),
        Line(
          points={{24,74},{24,-76}},
          color={95,95,95})}));
end SimDevTools;
