within SimDevTools.Functions;
function writeInteger "Write an Integer to an HDF5 file"
  input String fileName "File Name";
  input String datasetName "Dataset Name";
  input Integer value "Value";
  input String quantity = "" "Quantity (optional)";
  input String unit = "" "Unit (optional)";
  input String comment = "" "Comment (optional)";
protected
  Integer dims[:] = { 1};
  Integer data[1] = { value};
  external"C" ModelicaHDF5Functions_make_dataset_int(
          fileName,
          datasetName,
          1,
          dims,
          data,
          quantity,
          unit,
          comment) annotation (
  Include="#include \"ModelicaHDF5Functions.h\"",
  Library={"ModelicaHDF5Functions", "libhdf5", "libhdf5_hl"},
  IncludeDirectory="modelica://SimDevTools/Resources/Include",
  LibraryDirectory="modelica://SimDevTools/Resources/Library");
end writeInteger;
