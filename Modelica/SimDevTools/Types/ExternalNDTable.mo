within SimDevTools.Types;
class ExternalNDTable "External object of NDTable"
  extends ExternalObject;

  function constructor "Initialize table"
      input String filename;
      input String datasetname;
      input Integer ndims;
      input String datasetQuantity;
      input String datasetUnit;
      input String scaleQuantities[ndims];
      input String scaleUnits[ndims];
      input Boolean share;
      output ExternalNDTable externalTable;
  external"C" externalTable =
      ModelicaNDTable_open(
          filename,
          datasetname,
          ndims,
          datasetQuantity,
          datasetUnit,
          scaleQuantities,
          scaleUnits,
          share) annotation (
  Include="#include \"ModelicaNDTable.h\"",
  Library={"ModelicaNDTable", "libhdf5", "libhdf5_hl"},
  IncludeDirectory="modelica://SimDevTools/Resources/Include",
  LibraryDirectory="modelica://SimDevTools/Resources/Library");
  end constructor;

  function destructor "Close table"
    input ExternalNDTable externalTable;
  external"C" ModelicaNDTable_close(externalTable) annotation (
  Include="#include \"ModelicaNDTable.h\"",
  Library={"ModelicaNDTable", "libhdf5", "libhdf5_hl"},
  IncludeDirectory="modelica://SimDevTools/Resources/Include",
  LibraryDirectory="modelica://SimDevTools/Resources/Library");
  end destructor;

end ExternalNDTable;
