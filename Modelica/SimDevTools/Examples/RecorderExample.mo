within SimDevTools.Examples;
model RecorderExample
  extends Modelica.Icons.Example;

  Modelica.Electrical.Analog.Semiconductors.Diode diode annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-10,20})));
  Recorder recorder(
    nin=2,
    signalNames={"v","i"},
    signalQuantities={"Voltage","Current"},
    signalUnits={"V","A"},
    signalDisplayUnits={"mV",""},
    signalComments={"Diode voltage","Diode current"})
    annotation (Placement(transformation(extent={{60,-10},{80,10}})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-10,-20})));
  Modelica.Electrical.Analog.Sources.RampVoltage rampVoltage(V=1, duration=1)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-60,0})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={20,20})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-46,-60},{-26,-40}})));
equation
  connect(rampVoltage.p, diode.p) annotation (Line(
      points={{-60,10},{-60,40},{-10,40},{-10,30}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(voltageSensor.v, recorder.u[1]) annotation (Line(
      points={{30,20},{44,20},{44,-1},{58,-1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(currentSensor.i, recorder.u[2]) annotation (Line(
      points={{0,-20},{28,-20},{28,1},{58,1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(voltageSensor.p, diode.p) annotation (Line(
      points={{20,30},{-10,30}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(voltageSensor.n, diode.n) annotation (Line(
      points={{20,10},{-10,10}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(diode.n, currentSensor.p) annotation (Line(
      points={{-10,10},{-10,-10}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(currentSensor.n, ground.p) annotation (Line(
      points={{-10,-30},{-10,-40},{-36,-40}},
      color={0,0,255},
      smooth=Smooth.None));
  connect(ground.p, rampVoltage.n) annotation (Line(
      points={{-36,-40},{-60,-40},{-60,-10}},
      color={0,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end RecorderExample;
