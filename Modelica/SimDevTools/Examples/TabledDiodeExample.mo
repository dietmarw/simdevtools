within SimDevTools.Examples;
model TabledDiodeExample
  extends Modelica.Icons.Example;

  NDTable diode(
    nin=2,
    extrapMethod=SimDevTools.Types.ExtrapolationMethod.Linear,
    interpMethod=SimDevTools.Types.InterpolationMethod.Linear,
    dataQuantity="Current",
    dataset="/I_f",
    dataUnit="A",
    scaleQuantities={"Voltage","Temperature"},
    scaleUnits={"V","K"},
    filename=Modelica.Utilities.Files.loadResource("modelica://SimDevTools/Resources/Data/Examples/diode.sdf"))
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Sources.Ramp temperature(
    height=125,
    duration=1,
    offset=300)
    annotation (Placement(transformation(extent={{-60,10},{-40,30}})));
  Modelica.Blocks.Sources.Sine forwardVoltage(
    freqHz=100,
    amplitude=0.1,
    offset=1)
    annotation (Placement(transformation(extent={{-60,-30},{-40,-10}})));
equation
  connect(temperature.y, diode.u[2]) annotation (Line(
      points={{-39,20},{-26,20},{-26,1},{-12,1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(forwardVoltage.y, diode.u[1]) annotation (Line(
      points={{-39,-20},{-26,-20},{-26,-1},{-12,-1}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{
            100,100}}), graphics),
    experiment(__Dymola_NumberOfIntervals=5000),
    __Dymola_experimentSetupOutput);
end TabledDiodeExample;
