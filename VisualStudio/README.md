Install Build Tools
===================

- Download and install VisualStudio 2010 Express


Alternative 1: get the pre-build dependencies
=============================================

To use the pre-build dependencies for VisualStudio 2010 download [ThirdParty-prebuild.zip](https://bitbucket.org/modelon/simdevtools/downloads/ThirdParty-prebuilt.zip) and un-pack it to .../SimDevTools/ThirdParty.


Alternative 2: build the dependencies from source
=================================================

Install CMake
-------------

- Download and install/unpack [CMake](http://www.cmake.org/cmake/resources/software.html)


Build the HDF5 Libraries
--------------------------

- In the SimDevTools directory create a directory `ThirdParty`
- Download and unpack the [HDF5 Source Distribution](http://www.hdfgroup.org/ftp/HDF5/releases/hdf5-1.8.12/src/hdf5-1.8.12.zip) from the [HDF Group website](http://www.hdfgroup.org/HDF5/release/obtainsrc.html) to the `ThirdParty` directory
- Run the [CMake GUI](https://secure.mash-project.eu/wiki/index.php/CMake:_Getting_Started)

In CMake:

- choose the source code directory of the HDF5 source
- choose `.../SimDevTools/ThirdParty/HDF5-1.8.12/build` as the build directory
- click `Configure` and select `Visual Studio 10` and `Use default native compilers` and click `Finish`
- check the option `HDF5_BUILD_HL_LIB`
- click `Configure` again and then `Generate` to create the Visual Studio solution

In Visual Studio 2010:

- open the solution under .../SimDevTools/ThirdParty/hdf5-1.8.12/build/HDF5.sln
- right-click the on the `ALL_BUILD` project and select `Build` (for both Debug and Release)

Make sure you have the four static HDF5 libraries `libhdf5.lib` and `libhdf5_hl.lib` under .../SimDevTools/ThirdParty/hdf5-1.8.12/build/bin/Release and `libhdf5_D.lib` and `libhdf5_hl_D.lib` under .../SimDevTools/ThirdParty/hdf5-1.8.12/build/bin/Debug


Build the Google Test Libraries
-------------------------------

- download and unzip the [GTest source code](https://googletest.googlecode.com/files/gtest-1.7.0.zip) to the ThirdParty folder

In Visual Studio 2010:

- open the solution under .../SimDevTools/ThirdParty/gtest-1.7.0/msvc/gtest.sln
- for each project open the properties dialog. Under `Configuration Properties > C/C++ > Code Generation` change `Runtime Library` to `Multi-threaded DLL (/MD)` for the Release configuration and `Multi-threaded Debug DLL (/MDd)` for the Debug configuration.
- right-click the solution icon and select `Build Solution` (for both 'Debug' and 'Release' configurations)

Make sure you have the four static Google Test libraries `gtest_main.lib` and `gtest.lib` under .../SimDevTools/ThirdParty/gtest-1.7.0/msvc/gtest/Release and `gtest_maind.lib` and `gtestd.lib` under .../SimDevTools/ThirdParty/gtest-1.7.0/msvc/gtest/Debug

Copy the Modelica Utitlities Header
-----------------------------------

Copy the [Modelica Utilities header file](https://github.com/modelica/Modelica/blob/release/Modelica%203.2.1/Resources/C-Sources/ModelicaUtilities.h) to .../SimDevTools/ThirdParty/Resources/C-Sources


Build SimDevTools
----------------

In Visual Studio 2010:

- open the SimDevTools solution in Visual Studio 2010 (VisualStudio/SimDevTools.sln)
- enable the `Expert Settings` under `Tools > Settings > Expert Settings`
- in the Property Manager expand the first two levels of any project, right click `Microsoft.Cpp.Win32.user` and choose `Properties`
- under `Common Properties > User Macros` add the following macros (mind the trailing backslash '\'!):

`MatlabDir = <PATH_TO_MATLAB>` (if you have MATLAB installed)

Exporting Functional Mockup Units (FMUs) with Dymola
---------------------------------------------------

Dymola links against the static version of the C-runtime when exporting Models as FMUs. In order to include blocks or functions fromm the SimDevTools library into FMUs it is necessary to re-build all libraries (including the dependencies) using the `/MT flag (multi-threaded)` instead to the default `/MD (multi-threaded DLL)`. Make a backup copy of the .../SimDevTools/Modelica/SimDevTools/Resources/Library/win32 folder to be able to go back to the standart configuration.

- open the HDF5 solution and select the `Release`configuration
- for the `hdf5` and `hdf5_hl` projects change the code generation under `Configuration Properties > C/C++ > Code Generation` to `/MT (multi-threaded)`
- rebuild the `ALL_BUILD` project
- open the SimDevTools solution and change the code generation of the ModelicaHDF5Functions, ModelicaHDF5Recorder and ModelicaNDTable projects to `/MT flag (multi-threaded)` and rebuild the solution

Now you can export FMUs that contain SimDevTools blocks. To go back to the standart configuration rename the .../SimDevTools/Modelica/SimDevTools/Resources/Library/win32 to win32_mt and the copy the backup to win32.

To automate the switching of the libraries you can add a copy command to the `build.bat` and `buildfmu.bat` build files located in `<dymola installation>/bin` where "..." is the location of the SimDevTools Modelica library.

build.bat:

> copy /Y ...\SimDevTools\Resources\Library\win32_md\*.lib ...\SimDevTools\Resources\Library\win32\  

buildfmu.bat

> copy /Y ...\SimDevTools\Resources\Library\win32_mt\*.lib ...\SimDevTools\Resources\Library\win32\
