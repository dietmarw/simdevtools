
set zip="C:\Program Files\7-Zip\7z.exe"
set target="dist"
set archive="HDF5Table-0.0.1-SNAPSHOT.zip"
set msbuild="C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"
set archive="SimDevTools-0.1.3.zip"

set start_dir="%cd%"

REM clean up
rd /s /q %target%
del Python\sdf\*.pyc
del Python\ndtable\*.pyc

mkdir %target%\Modelica

REM build HDF5Table 
cd VisualStudio
%msbuild% HDF5Table.sln /p:Configuration=Release
cd ..

REM add the libraries
xcopy /e /i MATLAB %target%\MATLAB
xcopy /e /i Modelica %target%\Modelica
xcopy /e /i Python\sdf %target%\Python\sdf
xcopy /e /i Python\ndtable %target%\Python\ndtable
xcopy /e /i Simulink %target%\Simulink

copy README.md %target%
copy LICENSE.txt %target%

REM create the zip
cd %target%
%zip% a %archive% .
cd %start_dir%